# US Unlocked

On this Repo with scope only convert PSD to Responsive HTML using bootstrap framework!

#### **Installations**
Require NodeJS installed on your machine.

#### **Install Grunt **
> **`npm install -g grunt-cli`**
> or you can look at this http://gruntjs.com/getting-started for detail

#### **Install NodeJS Modules**
> **`npm install`**

#### **Compile**
> **`grunt`**
