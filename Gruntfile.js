/* -------------------------------------------
 *  GLOBAL MODULES, REQUIRE
/* ----------------------------------------- */
module.exports = function(grunt) {

	"use strict";
  
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

        /* UGLIFY CONFIGURATION
        /* ------------------------------------ */
    	uglify: {
    		global: {
    			files: {
    				"production/js/script.min.js": ["production/js/scripts.js"]
    			}
    		}
    	},

    	/* AUTOPREFIXER CONFIGURATION
    	/* ------------------------------------ */
    	autoprefixer:{
    		global: {
    			src: "production/css/app-unprefixed.css",
    			dest: "production/css/app.css"
    		}
    	}, 


    	/* SASS [LIBSASS] CONFIGURATION
    	/* ------------------------------------ */
        sass: {
        	global: {
        		options: {
                    sourceMap: true,
        			outputStyle: "expanded"
        		},
        		files: {
                    "production/css/app-unprefixed.css": "scss/application.scss",
        			"production/css/single-card.css": "scss/single-card.scss"
        		}
        	}
        },

        /* JSHINT CONFIGURATION
        /* ------------------------------------ */
        jshint: {
        	options: {
        		force: true
        	},
        	all: ['Gruntfile.js', 'production/js/scripts.js'],
        },

        /* INCLUDES CONFIGURATION
        /* ------------------------------------ */
        includes: {
            files: {
                cwd: 'site',
                src: [ '*.html', 'pages/*.html' ],
                dest: 'production/',
                options: {
                    silent: true,
                    includePath: 'include',
                    banner: '<!-- I am a banner <% includes.files.dest %> -->'
                }
            }
        },

        /* HTMLHINT CONFIGURATION
        /* ------------------------------------ */
        htmlhint: {
        	production:{
        		options:{
    	            'tag-pair': true,
    	            'tagname-lowercase': true,
    	            'attr-lowercase': true,
    	            'attr-value-double-quotes': true,
    	            'doctype-first': true,
    	            'spec-char-escape': true,
    	            'id-unique': true,
    	            'head-script-disabled': true,
    	            'style-disabled': true
        		}, // options
        		src: ['*.html']
        	}
        },

        /* IMAGEMIN CONFIGURATION
        /* ------------------------------------ */
        imagemin: {
            options: {
                optimizationLevel: 3,
                progressive: true,
            },
            dynamic: {
                expand: true,
                cwd: 'images/',
                src: ['**/*.{png,jpg,gif}'],
                dest: 'production/imagemin/'
            }
        },

        watch: {
    		options: {
                spawn: false,
    			livereload: true,
    		},

        	gruntfile: {
        		files: 'Gruntfile.js',
        		tasks: ['jshint:gruntfile'],
        	},

        	scripts: {
        		files: ['production/js/scripts.js'],
        		tasks: ['jshint', 'uglify'],
        	},

        	css: {
        		files: '**/*.scss',
        		tasks: ['sass', 'autoprefixer'],
        	}, // css

            html: {
                files: ['*.html'],
                tasks: ['htmlhint:production'],
            }, // html

            core: {
                files: [ 'include/*.html' , 'site/*.html' ],
                tasks: [ 'includes:files' ]
            },

            livereload: {
                files: ['*.html', '*.php', 'images/**/*.{png,jpg,jpeg,gif,webp,svg}']
            },

            images: {
                files: ['images/*.*'],
                tasks: ['imagemin'],
            },

        } // watch
    });

    require("load-grunt-tasks")(grunt);

    // REGISTER GRUNT TASKS
    grunt.registerTask( 'default', [ 'uglify', 'jshint', 'sass', 'autoprefixer', 'htmlhint', 'includes', 'watch'] );
    grunt.registerTask( 'serve', [ 'sass', 'autoprefixer', 'imagemin', 'watch'] );

};