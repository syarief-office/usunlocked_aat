$(document).ready(function() {
	"use strict";

	/* STICKY SIDEBAR-NAVIGATION
	/* ----------------------------- */
    /*$('#sidebar-nav').sticky({
        topSpacing: 45,
        className: 'sticky-sidebar'
    });*/

	/* BANNER-CAROUSEL CONFIGURATION
	/* ----------------------------- */
	$('.banners-carousel').owlCarousel({
		animateIn: 'fadeIn',
		animateOut: 'fadeOut',
		items: 1,
		smartSpeed: 450,
		autoplay: false,
		autoplayTimeout: 3000,
		loop: true,
	});

	/* TESTIMONIAL OWL-CAROUSEL CONFIGURATION
	/* ----------------------------- */
	$('.testimonial-carousel').owlCarousel({
		animateIn: 'fadeIn',
		animateOut: 'fadeOut',
		items: 1,
		smartSpeed: 450,
		autoplay: false,
		autoplayTimeout: 3000,
		loop: true,
		navText: [
				"<i class='fa fa-angle-left fa-2x'></i>",
            	"<i class='fa fa-angle-right fa-2x'></i>"
		],
		nav: true,
	});

	/* SELECT-FIELD CONFIGURATION
	/* ----------------------------- */
	$(".selectpicker").selectpicker({
		size: 4,
	});

	// MAP WIDGET
    $('#map-canvas').gmap({
        'center': '-6.94010,107.62575',
        'zoom': 18,
        scrollwheel: false,
        mapTypeId: google.maps.MapTypeId.HYBRID,
        'disableDefaultUI': false,
        'callback': function() {
            var self = this;
            self.addMarker({
                    'position': this.get('map').getCenter(),
                    icon: 'imagemin/marker-main.png',
                });
        }
    });
	
	
	// RWD-SIDEBAR MENU CONFIGURATION
	$(".main-navigation > ul").clone(false).find("ul,li").removeAttr("id").remove(".sub-menu").appendTo($(".rwd-navigation"));
	$(".top-navigation > ul").clone(false).removeAttr("id").remove(".sub-menu").appendTo($(".rwd-top-navigation ul"));
	$(".btn-rwd-sidebar, .btn-rwd-hide").click( function(e) {
		var rwd_container = $(".rwd-subcontainer");
		var rwd_sidebar = $(".rwd-nav-sidebar");
		rwd_container.toggleClass("rwd-active");
		rwd_sidebar.toggleClass("rwd-nav-active");
	});
	$(".btn-nav").click( function(e) {
		e.preventDefault();
		$(".rwd-navigation").slideToggle();
	});

	if(jQuery('#sidebar-nav').length){
		var $window = $(window),
			$el = jQuery('#sidebar-nav'),
			$gap = jQuery('#scrollingGap'),
		    stickyTop = $el.offset().top,
		    fooTop = $gap.offset().top,
		    elHeight = $el.innerHeight();
		jQuery(window).scroll(function(){
			var winTop = jQuery(window).scrollTop(),
			    limit = fooTop - elHeight - 20; 

			if($window.width() > 767){
				if(stickyTop < winTop){
					$el.css({
						'position' : 'fixed',
						'top' : 0
					});
				}else{
					$el.css('position', 'static');
				}
				if( limit < winTop){
					var diff = limit - winTop;
					$el.css({top: diff});
				}
			}
		});
	}

	$('.filterNav li a').click(function() {
	    var ourClass = $(this).attr('class');
	    $('.filterNav li').removeClass('active');
	    $(this).parent().addClass('active');
	    if(ourClass == 'filterAll') {
	      $('.filterHolder').children('div.filterItem').fadeIn(500);
	    }
	    else {
	      $('.filterHolder').children('div:not(.' + ourClass + ')').hide();
	      $('.filterHolder').children('div.' + ourClass).fadeIn(500);
	    }
	    return false;
  	});

  	if(jQuery('.scrollingSide').length){
		var $window = $(window),
			$el = jQuery('.scrollingSide'),
			$gap = jQuery('#scroll-limit'),
		    stickyTop = $el.offset().top,
		    fooTop = $gap.offset().top,
		    elHeight = $el.innerHeight();
		jQuery(window).scroll(function(){
			var winTop = jQuery(window).scrollTop(),
			    limit = fooTop - elHeight - 20; 

			if($window.width() > 767){
				if(stickyTop < winTop){
					$el.css({
						'position' : 'fixed',
						'top' : 0
					});
				}else{
					$el.css({
						'position' : 'absolute',
						'top' : 'auto'
					});
				}
				if( limit < winTop){
					var diff = limit - winTop;
					$el.css({top: diff});
				}
			}
		});
	};

	$('.list-secure a[data-toggle="tab"]').on('shown.bs.tab', function (e) {    
	    // var target = $(this).attr('href');

	    // $(target).css({'right':'-200px'});   
	    // var right = $(target).offset().right;
	    // $(target).css({right:right}).animate({"right":"0px"}, "300");
	});

	/** CUSTOM AUTOCOMPLETE
	===================================*/
	var dtShop = [
		"Everything But Water",
		"Palo Alto Software",
		"Vitamin Wagon",
		"Walgreens.com",
		"Watchsimo",
		"Apple Store",
		"Groovy",
		"Haskell",
		"Java",
		"JavaScript",
		"Lisp",
		"Perl",
		"PHP",
		"Python",
		"Ruby",
		"Scala",
		"Scheme"
	];
	var i,
	    shopNumb = dtShop.length;
	//ADD DOM TO shopResult 
	for(i=0; i< shopNumb; i++){
		$('#shopResult').append('<li><a href="#" target="_blank">'+ dtShop[i] +'</a></li>');
	};
	
	//COMPARING INPUT AND LIST
	var sortlist = function(){
		$('.sortInput').keyup( function () {
		    var filter = $(this).val(); // get the value of the input, which we filter on
		    if (filter) {
		      $('#shopResult').find("li:not(:contains(" + filter + "))").slideUp('fast');
		      $('#shopResult').find("li:contains(" + filter + ")").slideDown('fast');
		      $('#shopResult').slideDown('fast');
		    }else{
		    	$('#shopResult li').slideUp('fast');
		    	$('#shopResult').slideUp('fast');
		    }
		 });
	};
	sortlist();
	
	// $('#shopResult li').click(function(){
	// 	var text = $(this).html();
	// 	$('.sortInput').val(text);
	// 	$(this).parent().slideUp('fast');
	// });

	//CONTAINS : Case-insensitive
	$.expr[":"].contains = $.expr.createPseudo(function(arg) {
    	return function( elem ) {
       		return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
    	};
	});

	//TOGGLE FAQ
	$('.togContent:first').show();
	$('.togTrigger').click(function(){
		var $getParent = $(this).closest('li');
		$getParent.find('.togContent').slideToggle();
		return false;
	});



	//Toggle Verify pages
	
	$('.f-togTarget').each(function(){
		var getTarget = $(this).parent().find('.head-form-arr');
		if($(this).is(':visible')){
			getTarget.addClass('fa fa-angle-up');
		}else{
			getTarget.addClass('fa fa-angle-down');
		}
		// else{
		// 	verifyParent.find('.head-form-arr').addClass('fa fa-angle-down');
		// }
	});
	$('.head-form').click(function(){
		var togTarget = $(this).data('target'),
			arrows = $(this).find('.head-form-arr');
		$('.head-form-arr', this).toggleClass('fa fa-angle-down fa fa-angle-up');
		$(this).next($('.' + togTarget)).slideToggle('fast');
	});
	$('.nextStep').click(function(){
		var scrollTarget = $(this).closest('.verifyBundle').next();
		$('window, body').animate({
			scrollTop: scrollTarget.offset().top
		}, 500);
	});

	//SELECTED COUNTRY
	$('#countrySelect').bind('changed.bs.select', function(){
		var getVal = $('#countrySelect option:selected').val();
		getVal.toLowerCase();
		if(getVal === ('CHINA'.toLowerCase())){
			$('#countryAlert').slideDown('fast');
		}else{
			$('#countryAlert').slideUp('fast');
		}
	});

	$(window).on('load', function () {
		$('.main-navigation .btn-green').clone(false).prependTo('.banner-section').addClass('btnCard');
	});

});

$(window).load(function(){
	$("#status").fadeOut();
	$("#preloader").delay(450).fadeOut("slow");
	new WOW().init();
});

// equal hight
$(window).on("load", function() {
    var tallest = 0;
    if ($(window).width()) {
        $(".eqHeight").each(function() {
            var thisHeight = $(this).innerHeight();
            if (thisHeight > tallest) {
                tallest = thisHeight;
            }
        });
        $(".eqHeight").innerHeight(tallest);
    }
});

// tab proces
$(function(){

    $('#changetabbutton').click(function(e){
    	e.preventDefault();
        $('#mytabs a[href="#second"]').tab('show');
    })

    $('#changetabbutton2').click(function(e){
    	e.preventDefault();
        $('#mytabs a[href="#third"]').tab('show');
    })

    $('#changetabbutton3').click(function(e){
    	e.preventDefault();
        $('#mytabs a[href="#fourth"]').tab('show');
    })

    // $('#changetabbutton4').click(function(e){
    // 	e.preventDefault();
    //     $('.btn-non-active').addClass('active-button');
    // })

    // $('#changetabbutton4').click(function(e){
    // 	e.preventDefault();
    //     $('#mytabs a[href="#"]').tab('show');
    // })

});
