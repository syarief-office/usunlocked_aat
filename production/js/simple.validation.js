var input = document.getElementById('username'); // untuk field yang di check
var form  = document.getElementById('form'); // untuk form id
var elem               = document.createElement('div');
    elem.id            = 'notify';
    elem.style.display = 'none';
    form.appendChild(elem);


input.addEventListener('invalid', function(event) {
	event.preventDefault();
    if ( ! event.target.validity.valid ) {
        elem.textContent   = 'Username should only contain lowercase letters e.g. john';
        elem.className     = 'error';
        elem.style.display = 'block';
 
        input.className    = 'invalid animated shake';
    }
});

